import helpers.scrape_util as su


def get_data(file_path):
    data = su.channel() 
    l = su.lx_etree(file_path)
    data['entries'] = items(l)
    data['title'] = l.xpath("//title")[0].text
    return data


def items(etree):
    items = [parse(li) for li in etree.xpath("//div[@id='zox_side_trend_widget']/section")]
    return items


def parse(li):
    obj = su.item()
    obj['link'] = li.xpath('.//a')[0].attrib['href'] 
    obj['title'] = su.lx_text(li.xpath('.//h2')[0])
    obj['description'] = su.lx_text(li.xpath('.//p')[0])
    return obj
