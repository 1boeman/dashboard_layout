import helpers.scrape_util as su


def get_data(file_path):
    data = su.channel() 
    l = su.lx_etree(file_path)
    data['entries'] = items(l)
    data['title'] = l.xpath("//title")[0].text
    return data


def items(etree):
    items = [parse(li) for li in etree.xpath("//div[contains(@class,'summary-list__items')]")]
    return items


def parse(li):
    obj = su.item()
    obj['link'] = 'https://www.wired.com/category/security/#Latest'
    obj['title'] = su.lx_text(li.xpath('.//h3')[0])
    obj['description'] = su.lx_text(li.xpath('./div')[0])
    obj['pubDate'] = ""
    return obj
