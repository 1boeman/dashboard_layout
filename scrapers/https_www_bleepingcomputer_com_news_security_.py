import helpers.scrape_util as su


def get_data(file_path):
    data = su.channel() 
    l = su.lx_etree(file_path)
    data['entries'] = items(l)
    data['title'] = l.xpath("//title")[0].text
    return data


def items(etree):
    items = [parse(li) for li in etree.xpath("//ul[@id='bc-home-news-main-wrap']/li")]
    return items


def parse(li):
    obj = su.item()
    obj['link'] = li.xpath('.//a')[0].attrib['href'] 
    obj['title'] = su.lx_text(li.xpath('.//h4')[0])
    obj['description'] = su.lx_text(li.xpath('.//p')[0])
    obj['pubDate'] = li.xpath(".//ul/li[@class='bc_news_date']")[0].text
    return obj
