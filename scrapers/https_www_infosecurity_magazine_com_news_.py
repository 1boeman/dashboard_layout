import helpers.scrape_util as su


def get_data(file_path):
    data = su.channel() 
    l = su.lx_etree(file_path)
    data['entries'] = items(l)
    data['title'] = l.xpath("//title")[0].text
    return data


def items(etree):
    items = [parse(li) for li in etree.xpath("//ol[contains(@class,'webpages-list')]/li[contains(@class,'webpage-item')]")]
    return items


def parse(li):
    obj = su.item()
    obj['link'] = li.xpath('.//a')[0].attrib['href'] 
    obj['title'] = su.lx_text(li.xpath('.//h2')[0])
    obj['description'] = su.lx_text(li.xpath('.//p')[0])
    obj['pubDate'] = li.xpath(".//time")[0].text
    obj['enclosure']['url'] = li.xpath("//img[contains(@class,'webpage-thumb')]")[0].attrib['src']
    return obj
