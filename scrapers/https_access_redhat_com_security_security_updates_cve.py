import helpers.scrape_util as su


def get_data(file_path):
    data = su.channel() 
    l = su.lx_etree(file_path)
    data['entries'] = items(l)
    data['title'] = l.xpath("//title")[0].text
    return data


def items(etree):
    items = [parse(li) for li in etree.xpath("//cp-table[@id='cve-db-table']//cp-tr[@role='row']")]
    items = [i for i in items if i]
    return items


def parse(li):
    obj = su.item()
    if not len (li.xpath('.//a')):
        return False
    obj['link'] = li.xpath('.//a')[0].attrib['href'] 
    obj['title'] = su.lx_text(li.xpath('.//cp-th')[0])
    obj['description'] = su.lx_text(li.xpath(".//cp-td[contains(@data-label,'Description')]")[0])
    obj['pubDate'] = su.lx_text(li.xpath(".//pf-timestamp")[0])
    return obj
