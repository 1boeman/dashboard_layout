import helpers.scrape_util as su
import gzip
import json
import pprint

def get_data(file_path):
    with gzip.open(file_path,'rb') as f:
        _content = f.read()
        j = json.loads(_content)
    data = su.channel() 
    data['entries'] = items(j)
    data['title'] = 'NVD NIST'
    return data


def items(j):
    items = [parse(li) for li in j['CVE_Items']]
    return items


def parse(li):
    obj = su.item()
    if len(li['cve']['references']['reference_data']):
        obj['link'] = li['cve']['references']['reference_data'][0]['url']
    else:
        obj['link'] = '#'
    obj['title'] = li['cve']['CVE_data_meta']['ID']
    obj['description'] = li["cve"]["description"]["description_data"][0]["value"]
    return obj
